import numpy
from keras import backend as tf
from keras.utils import np_utils

# Import useful layers
from keras.models import Sequential
from keras.preprocessing.image import ImageDataGenerator
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras import backend as k

import json

model = Sequential()

# Specify dimension as last input
tf.set_image_data_format('channels_last')
numpy.random.seed(0)

# Data paths: train, test and validation
gen = ImageDataGenerator(validation_split=0.2)

# The absolute image path
folder_path = '/home/eirikgg/Pictures/New/Via-Dekkebilder_sorted_total_with_test/Train'

# Training bach size
_BATCH_SIZE_ = 10

# Image resize size
width = 400
height = 400

# Splits the images into two sets 
train = gen.flow_from_directory(folder_path, target_size=(width, height), batch_size=_BATCH_SIZE_, subset='training')
val = gen.flow_from_directory(folder_path, target_size=(width, height), batch_size=_BATCH_SIZE_, subset='validation')

# Class labels
labels = ['High_damage', 'Medium_damage', 'Low_damage', 'No_damage']

# Building model, first layer: convolution and max pooling
model.add(Conv2D(40, kernel_size=5, 
                 padding="same",
                 input_shape=(width, height, 3), 
                 activation = 'relu'))
model.add(MaxPooling2D(pool_size=(2, 2), 
                       strides=(2, 2)))

# Second layer convolution and max pooling
model.add(Conv2D(70, kernel_size=3, padding="same", activation = 'relu'))
model.add(Conv2D(500, kernel_size=3, padding="same", activation = 'relu'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

model.add(Conv2D(1024, kernel_size=3, padding="same", activation = 'relu'))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

# Flattens images and passes them to a fully connected layer
model.add(Flatten())
model.add(Dense(units=100, activation='relu'  ))
model.add(Dropout(0.1))
model.add(Dense(units=100, activation='relu'  ))
model.add(Dropout(0.1))
model.add(Dense(units=100, activation='relu'  ))
model.add(Dropout(0.3))

model.add(Dense(4))
model.add(Activation("softmax"))
model.compile(loss='categorical_crossentropy', 
              optimizer='adam', 
              metrics=['accuracy'])

# Training the model
hist=model.fit_generator(train, 
                    steps_per_epoch=len(train), 
                    epochs=25, 
                    validation_data=val, 
                    validation_steps=len(val))
model.save_weights('/home/eirikgg/classifiers/cnn/1_results/basic_cnn_26_04_2019/weights')

# Pretty format json
history = json.dumps(hist.history, indent=4, sort_keys=True)

# Prints history
print(history)

# Saves history
with open('/home/eirikgg/classifiers/cnn/1_results/basic_cnn_26_04_2019/results.json', 'w') as outfile:
    json.dump(hist.history, outfile)
